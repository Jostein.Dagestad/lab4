package datastructure;

import java.util.ArrayList;

import cellular.CellState;

public class CellGrid implements IGrid {

    public int cols;
    public int rows;
    CellState[][] cellGrid;



    public CellGrid(int rows, int columns, CellState initialState) {
        new ArrayList<CellGrid>();
        this.cols = columns;
        this.rows = rows;
        cellGrid = new CellState[rows][columns];



    }

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row < 0 || row >rows || column < 0 || column > cols) {
            throw new IndexOutOfBoundsException("nei nei index ikke med");
        }
        cellGrid [row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        if (row < 0 || row >rows || column < 0 || column > cols) {
            throw new IndexOutOfBoundsException("nei nei index ikke med ");
        }
        return cellGrid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid newGrid = new CellGrid(this.rows, this.cols, CellState.ALIVE);
        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < cols; column++) {
                newGrid.set(row, column, this.get(row, column));
            }
        }
        return newGrid;


    }

}
