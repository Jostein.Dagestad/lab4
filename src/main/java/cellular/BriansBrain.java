
package cellular;

import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Random;

public class BriansBrain implements CellAutomaton{
    IGrid currentGeneration;
    public BriansBrain(int rows, int columns){
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }

    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row, column);
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                } else {
                    currentGeneration.set(row, col, CellState.DEAD);
                }
            }
        }
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
        for (int i = 0; i < numberOfRows(); i++){
            for (int j = 0; j < numberOfColumns(); j++){
                CellState next = getNextCell(i, j);
                nextGeneration.set(i, j, next);
            }
        }
        currentGeneration = nextGeneration;
    }
    private int countNeighbors(int row, int col, CellState state) {
        // TODO
        int numNeighbors = 0;
        for (int i = -1; i < 2; i++){
            for (int j = -1; j < 2; j++){
                int newRow = row + i;
                int newCol = col + j;
                if ((newRow < 0) || (newRow >= currentGeneration.numRows()) ||
                        (newCol < 0) || (newCol >= currentGeneration.numColumns())){
                    continue;
                }
                else if ((newRow==row)&&(newCol==col)){
                    continue;
                }
                else if (getCellState(newRow, newCol).equals(state)){
                    numNeighbors+=1;
                }
            }
        }
        return numNeighbors;
    }
    @Override
    public CellState getNextCell(int row, int col) {
        CellState current = getCellState(row, col);
        int neighbors = countNeighbors(row, col, CellState.ALIVE);
        CellState next = CellState.DEAD;
        if (current.equals(CellState.DEAD) && neighbors==2){
            next = CellState.ALIVE;
        }
        else if (current.equals(CellState.DYING)){
            next = CellState.DEAD;
        }
        else if (current.equals(CellState.ALIVE)){
            next = CellState.DYING;
        }
        return next;
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
}